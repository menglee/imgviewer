import java.io.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import javax.imageio.*;

public class JIMachine
{
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				JFrame frame = new ImageFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}

class ImageFrame extends JFrame
{
	private File file;
	private JFileChooser chooser;
	private BufferedImage image;
	private JPanel buttonPanel;
	private JButton openButton;
	private JButton zoomInButton;
	private JButton oneHundredButton;
	private JButton zoomOutButton;
	private JButton quitButton;
	private ImagePanel imgPane;
	private int defaultScreenHeight;
	private int defaultScreenWidth;
	private int zoomHeight;
	private int zoomWidth;
	private final double ZOOM_FACTOR = 0.25;
	
	public ImageFrame()
	{
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		defaultScreenHeight = (screenSize.height) / 2;
		defaultScreenWidth = (screenSize.width) / 2;
		
		setSize(defaultScreenWidth, defaultScreenHeight);
		setLocationByPlatform(true);
		setLayout(new BorderLayout());
		
		add(makeButtonPanel(), BorderLayout.SOUTH);
		
		// registering event handlers
		EventHandler handler = new EventHandler();
		openButton.addActionListener(handler);
		zoomInButton.addActionListener(handler);
		oneHundredButton.addActionListener(handler);
		zoomOutButton.addActionListener(handler);
		quitButton.addActionListener(handler);
		
		imgPane = new ImagePanel();
		add(imgPane, BorderLayout.CENTER);
	}
	
	public JPanel makeButtonPanel()
	{
		buttonPanel = new JPanel();
		
		openButton = new JButton("Open");
		buttonPanel.add(openButton);
		
		zoomInButton = new JButton("Zoom In");
		buttonPanel.add(zoomInButton);
		
		oneHundredButton = new JButton("100%");
		buttonPanel.add(oneHundredButton);
		
		zoomOutButton = new JButton("Zoom Out");
		buttonPanel.add(zoomOutButton);
		
		quitButton = new JButton("Quit");
		buttonPanel.add(quitButton);
		
		return buttonPanel;
	}
	
	private class EventHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if (event.getSource() == openButton)
			{
				chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("."));
				int result = chooser.showOpenDialog(ImageFrame.this);
				if (result == JFileChooser.APPROVE_OPTION)
				{
					file = chooser.getSelectedFile();
					zoomWidth = defaultScreenWidth;
					zoomHeight = defaultScreenHeight;
					imgPane.readImage(file);
					imgPane.repaint();
				}
			}
			else if (event.getSource() == zoomInButton)
			{
				int currWidth = zoomWidth;
				int currHeight = zoomHeight;
				currWidth = (int)(currWidth * ZOOM_FACTOR) + currWidth;
				currHeight = (int)(currHeight * ZOOM_FACTOR) + currHeight;
				zoomWidth = currWidth;
				zoomHeight = currHeight;
				imgPane.repaint();
			}
			else if (event.getSource()  == oneHundredButton)
			{
				zoomWidth = image.getWidth();
				zoomHeight = image.getHeight();
				imgPane.repaint();
			}
			else if (event.getSource() == zoomOutButton)
			{
				int currWidth = zoomWidth;
				int currHeight = zoomHeight;
				currWidth = currWidth - (int)(currWidth * ZOOM_FACTOR);
				currHeight = currHeight - (int)(currHeight * ZOOM_FACTOR);
				zoomWidth = currWidth;
				zoomHeight = currHeight;
				imgPane.repaint();
			}
			else if (event.getSource() == quitButton)
			{
				System.exit(0);
			}
		}
	}
	
	public class ImagePanel extends JPanel
	{
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			if (image != null)
				g.drawImage(image, 0, 0, zoomWidth, zoomHeight, this);
			
		}
		
		public void readImage(File f)
		{
			try {
				image = ImageIO.read(f);
			}
			catch (IOException x) {}
		}
	}
}